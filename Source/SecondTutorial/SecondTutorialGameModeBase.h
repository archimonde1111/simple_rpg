// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SecondTutorialGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SECONDTUTORIAL_API ASecondTutorialGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
