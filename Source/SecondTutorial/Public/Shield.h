// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment.h"
#include "Shield.generated.h"

/**
 * 
 */
UCLASS()
class SECONDTUTORIAL_API AShield : public AEquipment
{
	GENERATED_BODY()
	
public:
	AShield();
};
