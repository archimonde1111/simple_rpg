// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment.h"
#include "Weapon.generated.h"

class UBoxComponent;
class USoundCue;

/**
 * 
 */
UCLASS()
class SECONDTUTORIAL_API AWeapon : public AEquipment
{
	GENERATED_BODY()

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item | Combat")
		UBoxComponent* CombatCollision = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Combat")
		TSubclassOf<UDamageType> DamageTypeClass = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Combat")
		float Damage = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Sound")
		USoundCue* SwingSound = nullptr;


protected:
	void BeginPlay() override;

	virtual void Collision_Component_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) override;
	virtual void Collision_Component_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	UFUNCTION()
		virtual void Combat_Collision_Component_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	UFUNCTION()
		virtual void Combat_Collision_Component_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:
	AWeapon();

	FORCEINLINE	float Get_Weapon_Damage() { return Damage; }

	UFUNCTION(BlueprintCallable)
		void Enable_Combat_Collision();
	UFUNCTION(BlueprintCallable)
		void Disable_Combat_Collision();

	UFUNCTION(BlueprintCallable)
		void Play_SwingSound();
};
