// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "MainAnimInstance.generated.h"

class APawn;
class AMainCharacter;

/**
 * 
 */
UCLASS()
class SECONDTUTORIAL_API UMainAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimationProperties")
		float MovementSpeed = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimationProperties")
		bool bIsInAir = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimationProperties")
		APawn* Pawn = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimationProperties")
		AMainCharacter* MainCharacter = nullptr;

public:
	virtual void NativeInitializeAnimation() override;
	UFUNCTION(BlueprintCallable, Category = "AnimationProperties")
		void UpdateAnimationProperties();
	UFUNCTION(BlueprintPure, BlueprintCallable, Category = "AnimationProperties")
		bool Is_Armed();

};
