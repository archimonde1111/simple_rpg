// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class AItem;
class AEquipment;
class AWeapon;
class AShield;
class UAnimMontage;
class UParticleSystem;
class USoundCue;
class AEnemyCharacter;
class AMainPlayerController;


UENUM(BlueprintType)
enum class EMovementStatus : uint8
{
	EMS_Walking UMETA(DisplayName = "Walking"),
	EMS_Sprinting UMETA(DisplayName = "Sprinting"),
	EMS_Dead UMETA(DisplayName = "Dead"),

	EMS_MAX UMETA(DisplayName = "DefaultMAX", Hidden)
};

UENUM(BlueprintType)
enum class EStaminaStatus : uint8
{
	ESS_Rested UMETA(DisplayName = "Rested"),
	ESS_Tired UMETA(DisplayName = "Tired"),
	ESS_Exhausted UMETA(DisplayName = "Exhausted"),
	ESS_ExhaustedRecovering UMETA(DisplayName = "ExhaustedRecovering"),

	ESS_MAX UMETA(DisplayName = "DefaultMAX", Hidden)
};


UENUM(BlueprintType)
enum class EEquipmentType : uint8
{
	EET_Weapon UMETA(DisplayName = "Weapon"),
	EET_Shield UMETA(DisplayName = "Shield"),

	EET_MAX UMETA(DisplayName = "DefaultMAX", Hidden)
};


UCLASS()
class SECONDTUTORIAL_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMainCharacter();

protected:
	virtual void BeginPlay() override;

	void Set_MovementStatus(EMovementStatus Status);
	FORCEINLINE void Set_StaminaStatus(EStaminaStatus Status) { StaminaStatus = Status; }
	FORCEINLINE void Set_InterpolateToEnemy(bool bInterpolate) { bInterpolateToEnemy = bInterpolate; }

	void Change_Movement_Status();
	void Handle_Stamina_Change(float DeltaTime);
	void Regenerate_Stamina(float DeltaTime);
	void Drain_Stamina(float DeltaTime);

	void Move_Forward(float Value);
	void Move_Right(float Value);

	/**Turn rate with arrow keys*/
	void Turn_At_Rate(float Rate);
	void LookUp_At_Rate(float Rate);

	void ShiftKey_Up();
	void ShiftKey_Down();
	
	
	void Start_Attacking();
	FRotator Get_Look_At_Rotation_Yaw(FVector const& TargetLocation);
	void Attack();
	UFUNCTION(BlueprintCallable, Category = "Animations | Combat")
		void Attack_End();
	void Stop_Attacking();


	void Use_Item();


	void Character_Killed();
	UFUNCTION(BlueprintCallable, Category = "Animations | Combat")
		void Death_End();
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
		USpringArmComponent* CameraBoom = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
		UCameraComponent* Camera = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Items")
		AWeapon* EquippedWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Items")
		AShield* EquippedShield = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Items")
		AItem* ActiveOverlappingItem = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat")
		AEnemyCharacter* CombatTarget = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Combat")
		FVector CombatTargetLocation = FVector(0.f);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerController")
		AMainPlayerController* PlayerController = nullptr;

	/**How fast you will turn with arrow keys*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
		float BaseTurnRate = 65.f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
		float BaseLookUpRate = 65.f;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerStats | Health")
		float MaxHealth = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerStats | Health")
		float Health = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Enums")
		EStaminaStatus StaminaStatus = EStaminaStatus::ESS_Rested;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PlayerStats | Stamina")
		float MaxStamina = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PlayerStats | Stamina")
		float Stamina = 0.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PlayerStats | Stamina")
		float TiredStamina = 50.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PlayerStats | Stamina")
		float StaminaRecovery = 10.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PlayerStats | Stamina")
		float StaminaDrain = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PlayerStats")
		uint8 Coins = 0;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Enums")
		EMovementStatus MovementStatus;
	bool bShiftDown = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Running")
		float WalkingSpeed = 375.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Running")
		float SprintingSpeed = 500.f;
	
	float InterpolationSpeedWhenRotating = 15.f;
	bool bInterpolateToEnemy = false;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Animations | Combat")
		bool StartAttacking = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Animations | Combat")
		bool bAttacking = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations | AnimationMontages | Combat")
		UAnimMontage* CombatMontage = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations | AnimationMontages | Combat")
		TArray<FName> CombatMontageSections;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations | AnimationMontages | Combat")
		bool bMontageHaveDeathSection = false;
	/**
	 * Counting from 0 (if you have 3 sections last one number is 2)
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations | AnimationMontages | Combat")
		uint8 DeathSectionNumber = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects | VisualEffects")
		UParticleSystem* HitParticles = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects | SoundEffects")
		USoundCue* HitSound = nullptr;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	FORCEINLINE USpringArmComponent* Get_Camera_Boom() const { return CameraBoom; }
	FORCEINLINE UCameraComponent* Get_Camera() const { return Camera; }

	void Set_Equipment(AEquipment* Equipment, EEquipmentType Type);
	FORCEINLINE void Set_ActiveOverlappingItem(AItem* Item) { ActiveOverlappingItem = Item; }
	FORCEINLINE void Set_CombatTarget(AEnemyCharacter* Target) { CombatTarget = Target; }
	FORCEINLINE AEnemyCharacter* Get_CombatTarget() const { return CombatTarget; }

	FORCEINLINE AMainPlayerController* Get_PlayerController() const { return PlayerController; }

	void Update_CombatTarget();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	void Decrement_Health(float Damage);
	void Increment_Coins(uint8 CoinNumber);

	bool Is_Armed();

	void Spawn_HitParticles(FVector const& SpawnLocation, FRotator const& SpawnRotation) const;
	void Play_HitSound() const;

	virtual void Jump() override;
};
