// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

class UUserWidget;

/**
 * 
 */
UCLASS()
class SECONDTUTORIAL_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()


protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<UUserWidget> HUDOverlayAsset;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Widgets")
		UUserWidget* HUDOverlay = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<UUserWidget> WidgetEnemyHealthBar = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Widgets")
		UUserWidget* EnemyHealthBar = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Widgets")
		FVector EnemyLocation = FVector(0.f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		FVector2D EnemyHealthBarSizeInViewport = FVector2D(250.f, 25.f);

protected:
	virtual void BeginPlay();

	virtual void Tick(float DeltaTime) override;

public:
	void Display_EnemyHealthBar(bool ShouldDisplay = true);

	FORCEINLINE void Set_EnemyLocation(FVector Location) { EnemyLocation = Location; }

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Widgets")
		bool bEnemyHealthBarVisible = false;

	
};
