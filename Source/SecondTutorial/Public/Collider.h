// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Collider.generated.h"

class USphereComponent;
class UCameraComponent;
class USpringArmComponent;
class UColliderMovementComponent;

UCLASS()
class SECONDTUTORIAL_API ACollider : public APawn
{
	GENERATED_BODY()

public:
	ACollider();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, Category = "Mesh");
		UStaticMeshComponent* MeshComponent = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Mesh");
		USphereComponent* SphereComponent = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Movement")
		UColliderMovementComponent* OurMovementComponent = nullptr;

	UPROPERTY(EditDefaultsOnly)
	USpringArmComponent* SpringArm = nullptr;
	UCameraComponent* Camera = nullptr;

	void Move_Forward(float Axis);
	void Move_Right(float Axis);

	void Camera_Pitch(float Axis);
	void Camera_Yaw(float Axis);
	FVector2D CameraInput = FVector2D(0.f);

//CHECK HOW IT WORKS IN UE
	FORCEINLINE UStaticMeshComponent& Get_MeshComponent() { return *MeshComponent; }
	FORCEINLINE void Set_MeshComponent(UStaticMeshComponent& StaticMeshComponent) { MeshComponent = &StaticMeshComponent; }

	FORCEINLINE USphereComponent& Get_SphereComponent() { return *SphereComponent; }
	FORCEINLINE void Set_SphereComponent(USphereComponent& StaticSphereComponent) { SphereComponent = &StaticSphereComponent; }

	FORCEINLINE UCameraComponent& Get_CameraComponent() { return *Camera; }
	FORCEINLINE void Set_CameraComponent(UCameraComponent& CameraComponent) { Camera = &CameraComponent; }

	FORCEINLINE USpringArmComponent& Get_SpringArmComponent() { return *SpringArm; }
	FORCEINLINE void Set_SpringArmComponent(USpringArmComponent& SpringArmComponent) { SpringArm = &SpringArmComponent; }

	virtual UPawnMovementComponent* GetMovementComponent() const override;
};
