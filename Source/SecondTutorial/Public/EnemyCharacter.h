// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"

class USphereComponent;
class AAIController;
class AMainCharacter;
class UParticleSystemComponent;
class USoundCue;
class UBoxComponent;
class UAnimMontage;


UENUM(BlueprintType)
enum class EEnemyMovementStatus : uint8
{
	EEMS_Idle UMETA(DisplayName = "Idle"),
	EEMS_MoveToTarget UMETA(DisplayName = "MoveToTarget"),
	EEMS_Attacking UMETA(DisplayName = "Attacking"),
	EEMS_Dead UMETA(DisplayName = "Dead"),

	EEMS_MAX UMETA(DisplayName = "DefaultMAX", Hidden)
};

UCLASS()
class SECONDTUTORIAL_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AEnemyCharacter();

protected:
	virtual void BeginPlay() override;
	void Setup_CombatCollisions_Properties();

	UFUNCTION()
		virtual void AgroSphere_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	UFUNCTION()
		virtual void AgroSphere_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		virtual void AttackRangeSphere_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	UFUNCTION()
		virtual void AttackRangeSphere_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void CombatCollision_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	UFUNCTION()
		void CombatCollision_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	
	UFUNCTION(BlueprintCallable)
		void Move_To_Target(AMainCharacter* Target);

	void Start_Attack();
	void Draw_Attack_Type(UAnimInstance& AnimInstance, uint8 const& NumberOfNormalAttackAnimations);
	UFUNCTION(BlueprintCallable)
		void Attack_End();

	void Play_VFX(const AMainCharacter& Character);

	void Decrement_Health(float Damage, AActor* DamageCauser);
	void Character_Killed(AActor* Killer);
	UFUNCTION(BlueprintCallable)
		void Death_End();
	bool Is_Dead();
	void Disappear();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		EEnemyMovementStatus EnemyMovementStatus = EEnemyMovementStatus::EEMS_Idle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
		AAIController* AIController = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
		USphereComponent* AgroSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
		USphereComponent* AttackRangeSphere = nullptr;
	/**
	 * Array filled in blueprint construction script
	 * Filled in blueprint so it's easier to add different collisions if enemy can attack with more than one "weapon".
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI | Combat")
		TArray<UBoxComponent*> CombatCollisions;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI | Combat")
		float DeathDelay = 2.f;
	FTimerHandle DeathTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI | Combat")
		float Health = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI | Combat")
		float MaxHealth = 50.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI | Combat")
		float Damage = 10.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI | Combat")
		TSubclassOf<UDamageType> DamageTypeClass = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI | Combat")
		UParticleSystem* HitParticles = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AI")
		bool bOverlappingAttackRangeSphere = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
		AMainCharacter* CombatTarget = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI | Combat")
		bool bAttacking = false;

	FTimerHandle AttackTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI | Combat")
		float MinimumTimeBetweenAttacks = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI | Combat")
		float MaximumTimeBetweenAttacks = 1.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI | Sound")
		USoundCue* SwingSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI | Sound")
		USoundCue* HitSound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations | AnimationMontages")
		UAnimMontage* CombatMontage = nullptr;
	/**
	 * Sections shuld have the same ordinal number as BloodSplash_Socket && CombatCollision, that will colide when animation is played
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations | AnimationMontages")
		TArray<FName> CombatMontageSections;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations | AnimationMontages")
		bool bHaveComboAttack = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations | AnimationMontages")
		bool bMontageHaveDeathSection = false;
	/**
	 * Counting from 0 (if you have 3 sections last one number is 2)
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations | AnimationMontages", meta = (EditCondition = "bHaveComboAttack"))
		uint8 ComboSectionNumber = 0;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations | AnimationMontages", meta = (EditCondition = "bMontageHaveDeathSection"))
		uint8 DeathSectionNumber = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, CAtegory = "Animations | AnimationMontages")
		float AttackAnimationSpeed = 1.f;
	uint32 SectionToPlayNumber = 0;

public:	
	virtual void Tick(float DeltaTime) override;

	FORCEINLINE void Set_EnemyMovementStatus(EEnemyMovementStatus Status) { EnemyMovementStatus = Status; }
	FORCEINLINE void Set_Reference_To_AIController(AAIController* ControllerToSet) { AIController = ControllerToSet; }

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	void Spawn_HitParticles(FVector const SpawnLocation, FRotator const SpawnRotation);
	void Play_HitSound();

	UFUNCTION(BlueprintCallable)
		void Enable_Combat_Collision();
	UFUNCTION(BlueprintCallable)
		void Disable_Combat_Collision();
	UFUNCTION(BlueprintCallable)
		void Play_SwingSound();

public:
	bool bHasValidTarget = false;

};
