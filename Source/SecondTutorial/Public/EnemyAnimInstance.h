// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "EnemyAnimInstance.generated.h"

class APawn;
class AEnemyCharacter;

/**
 * 
 */
UCLASS()
class SECONDTUTORIAL_API UEnemyAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimationProperties")
		float MovementSpeed = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimationProperties")
		APawn* Pawn = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimationProperties")
		AEnemyCharacter* EnemyCharacter = nullptr;

public:
	virtual void NativeInitializeAnimation() override;

	UFUNCTION(BlueprintCallable, Category = "AnimationProperties")
		void UpdateAnimationProperties();
};
