// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnVolume.generated.h"

class UBoxComponent;
class AActor;

UCLASS()
class SECONDTUTORIAL_API ASpawnVolume : public AActor
{
	GENERATED_BODY()
	
public:	
	ASpawnVolume();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spawning")
		UBoxComponent* SpawningBox = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Spawning")
		uint8 Number_Of_Actors_To_Spawn = 1;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Spawning")
		TArray<TSubclassOf<AActor>> ActorsToSpawn; 
public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure, Category = "Spawning")
		FVector Get_Random_Spawn_Point();
	UFUNCTION(BlueprintPure, Category = "Spawning")
		TSubclassOf<AActor> Get_Random_Actor_To_Spawn();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, CAtegory = "Spawning")
		void Spawn_Actor(UClass* ClassToSpawn, const FVector& Location);
};
