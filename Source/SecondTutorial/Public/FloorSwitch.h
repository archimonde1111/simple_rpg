// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloorSwitch.generated.h"

class UBoxComponent;
class UStaticMeshComponent;

UCLASS()
class SECONDTUTORIAL_API AFloorSwitch : public AActor
{
	GENERATED_BODY()
	
public:	
	AFloorSwitch();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor Switch")
		UBoxComponent* FloorSwitchTriggerBox = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor Switch")
		UStaticMeshComponent* FloorSwitch = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor Switch")
		UStaticMeshComponent* DoorToOpen = nullptr;

	UPROPERTY(BlueprintReadWrite, Category = "Floor Switch")
		FVector DoorInitialLocation = FVector(0.f);
	UPROPERTY(BlueprintReadWrite, Category = "Floor Switch")
		FVector FloorSwitchInitialLocation = FVector(0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Floor Switch")
		float SwitchTime = 2.f;
	FTimerHandle SwitchHandle;
	void Close_Door();
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void On_Overlap_Begin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void On_Overlap_End(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintImplementableEvent, Category = "Floor Switch")
		void Rise_Door();
	UFUNCTION(BlueprintImplementableEvent, Category = "Floor Switch")
		void Lower_Door();
	UFUNCTION(BlueprintImplementableEvent, Category = "Floor Switch")
		void Raise_FloorSwitch();
	UFUNCTION(BlueprintImplementableEvent, Category = "Floor Switch")
		void Lower_FloorSwitch();
	
	UFUNCTION(BlueprintCallable, Category = "Floor Switch")
		void Update_Door_Location(float Z);
	UFUNCTION(BlueprintCallable, Category = "Floor Switch")
		void Update_FloorSwitch_Location(float Z);
};
