// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloatingPlatform.generated.h"

class UStaticMeshComponent;

UCLASS()
class SECONDTUTORIAL_API AFloatingPlatform : public AActor
{
	GENERATED_BODY()
	
	void Toggle_Interping();
	void Swap_Vectors(FVector& FirstVector, FVector& SecondVector);
public:	
	AFloatingPlatform();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Flaoting Platform")
		UStaticMeshComponent* Platform = nullptr;
		
	UPROPERTY(EditAnywhere)
		FVector StartLocation = FVector(0.f);
	UPROPERTY(EditAnywhere, meta = (MakeEditWidget = "true"))
		FVector EndLocation = FVector(0.f);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Floating Platform")
		float InterpSpeed = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Floating Platform")
		float InterpTime = 1.f;
	FTimerHandle InterpTimer;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Floating Platform")
		bool bInterping = false;

public:	
	virtual void Tick(float DeltaTime) override;

};
