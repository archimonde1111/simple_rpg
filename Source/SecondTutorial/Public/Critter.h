// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Critter.generated.h"

UCLASS()
class SECONDTUTORIAL_API ACritter : public APawn
{
	GENERATED_BODY()

	void Move_Forward(float Value);
	void Move_Right(float Value);
public:
	ACritter();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	UPROPERTY(EditDefaultsOnly)
		class USkeletalMeshComponent* SkeletalMeshComponent = nullptr;

private:
	FVector CurrentVelocity = FVector(0.f);
	UPROPERTY(EditAnywhere, Category = "Movement")
		float MaxSpeed = 100.f;
};
