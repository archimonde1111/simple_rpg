// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "MainCharacter.h"
#include "Equipment.generated.h"

class USkeletalMeshComponent;
class USoundCue;

UENUM(BlueprintType)
enum class EEquipmentState : uint8
{
	EES_Pickup UMETA(DisplayName = "Pickup"),
	EES_Equipped UMETA(DisplayName = "Equipped"),

	EES_MAX UMETA(DisplayName = "DefaultMax")
};

/**
 * 
 */
UCLASS()
class SECONDTUTORIAL_API AEquipment : public AItem
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
		FName EquipmentSocketName = FName("EquipmentSocket");
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		EEquipmentState EquipmentState = EEquipmentState::EES_Pickup;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		EEquipmentType EquipmentType = EEquipmentType::EET_Weapon;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item | SkeletalMesh")
		USkeletalMeshComponent* EquipmentSkeletalMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Sound")
		USoundCue* EquippingSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Particles")
		bool bEquipmentParticles = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item | Combat")
		AController* EquipmentInstigator = nullptr;
	
protected:
	virtual void Collision_Component_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) override;
	virtual void Collision_Component_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	FORCEINLINE void Set_EquipmentInstigator(AController* Inst) { EquipmentInstigator = Inst; }

public:
	AEquipment();
	virtual void Equip(AMainCharacter* MainCharacter);

	FORCEINLINE void Set_EquipmentState(EEquipmentState State) { EquipmentState = State; }
	FORCEINLINE EEquipmentState Get_EquipmentState() { return EquipmentState; }
	
};
