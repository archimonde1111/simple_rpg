// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAnimInstance.h"
#include "EnemyCharacter.h"



void UEnemyAnimInstance::NativeInitializeAnimation()
{
    if(!Pawn)
    {
        Pawn = TryGetPawnOwner();
        if(Pawn)
        {
            EnemyCharacter = Cast<AEnemyCharacter> (Pawn);
        }
    }
}


void UEnemyAnimInstance::UpdateAnimationProperties()
{
    if(!Pawn)
    {
        Pawn = TryGetPawnOwner();
        if(Pawn)
        {
            EnemyCharacter = Cast<AEnemyCharacter> (Pawn);
        }
    }
    if(!Pawn) { return; }

    FVector Speed = Pawn->GetVelocity();
    FVector Speed2D = FVector(Speed.X, Speed.Y, 0.f);
    MovementSpeed = Speed2D.Size();
}