// Fill out your copyright notice in the Description page of Project Settings.


#include "MainAnimInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MainCharacter.h"


void UMainAnimInstance::NativeInitializeAnimation()
{
    if(!Pawn)
    {
        Pawn = TryGetPawnOwner();
        MainCharacter = Cast<AMainCharacter> (Pawn);
    }
}

void UMainAnimInstance::UpdateAnimationProperties()
{
    if(!Pawn) 
    { 
        Pawn = TryGetPawnOwner();
    }
    if(!Pawn) { return; }

    FVector Speed = Pawn->GetVelocity();
    FVector Speed2D = FVector(Speed.X, Speed.Y, 0.f);
    MovementSpeed = Speed2D.Size();

    bIsInAir = Pawn->GetMovementComponent()->IsFalling();
}

bool UMainAnimInstance::Is_Armed()
{
    if(!MainCharacter)
    {
        if(!Pawn)
        {
            Pawn = TryGetPawnOwner();
        }

        MainCharacter = Cast<AMainCharacter> (Pawn);
    }
    if(!MainCharacter) { return false; }

    return MainCharacter->Is_Armed();
}


