//

#include "MainCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Sound/SoundCue.h"
#include "Engine/World.h"
#include "Item.h"
#include "Equipment.h"
#include "Weapon.h"
#include "Shield.h"
#include "EnemyCharacter.h"
#include "MainPlayerController.h"


AMainCharacter::AMainCharacter()
{
    CameraBoom = CreateDefaultSubobject<USpringArmComponent> (FName("CameraBoom"));
    CameraBoom->SetupAttachment(GetRootComponent());
    CameraBoom->SetRelativeLocation(FVector(0.f, 0.f, 30.f));
    CameraBoom->TargetArmLength = 600.f;
    CameraBoom->bUsePawnControlRotation = true;

    Camera = CreateDefaultSubobject<UCameraComponent> (FName("Camera"));
    Camera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    Camera->bUsePawnControlRotation = false;

    GetCapsuleComponent()->SetCapsuleSize(38.f, 85.f);
    bUseControllerRotationPitch = false;
    bUseControllerRotationRoll = false;
    bUseControllerRotationYaw = false;

    GetCharacterMovement()->bOrientRotationToMovement = true;// Character moves in the direction of input...
    GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.f, 0.0f);// ...at this rotation rate
    GetCharacterMovement()->JumpZVelocity = 450.f;
    GetCharacterMovement()->AirControl = 0.2f; //Control over pawn in air
}

void AMainCharacter::BeginPlay()
{
    Super::BeginPlay();

    Health = MaxHealth;
    Stamina = MaxStamina;

    PlayerController = Cast<AMainPlayerController> (GetController());
}

void AMainCharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if(MovementStatus == EMovementStatus::EMS_Dead) { return; }

    Change_Movement_Status();
    Handle_Stamina_Change(DeltaTime);

    if(bInterpolateToEnemy && CombatTarget)
    {
        FRotator PlayerToTargetRotationYaw = Get_Look_At_Rotation_Yaw(CombatTarget->GetActorLocation());

        FRotator InterpRotation = FMath::RInterpTo(GetActorRotation(), PlayerToTargetRotationYaw, DeltaTime, InterpolationSpeedWhenRotating);
        SetActorRotation(InterpRotation);
    }

    if(CombatTarget)
    {
        CombatTargetLocation = CombatTarget->GetActorLocation();
        if(PlayerController)
        {
            PlayerController->Set_EnemyLocation(CombatTargetLocation);
        }
    }
}
void AMainCharacter::Change_Movement_Status()
{
    switch (StaminaStatus)
    {
        case EStaminaStatus::ESS_Rested:
            if(bShiftDown)
            {
                Set_MovementStatus(EMovementStatus::EMS_Sprinting);
            }
            else
            {
                Set_MovementStatus(EMovementStatus::EMS_Walking);
            }
            break;

        case EStaminaStatus::ESS_Tired:
            if(bShiftDown)
            {
                Set_MovementStatus(EMovementStatus::EMS_Sprinting);
            }
            else
            {
                Set_MovementStatus(EMovementStatus::EMS_Walking);
            }
            break;

        case EStaminaStatus::ESS_Exhausted:
            Set_MovementStatus(EMovementStatus::EMS_Walking);
            break;

        case EStaminaStatus::ESS_ExhaustedRecovering:
            Set_MovementStatus(EMovementStatus::EMS_Walking);
            break;

        default:
            break;
    }
}
void AMainCharacter::Set_MovementStatus(EMovementStatus Status)
{
    MovementStatus = Status;
    switch(Status)
    {
        case EMovementStatus::EMS_Walking:
            GetCharacterMovement()->MaxWalkSpeed = WalkingSpeed;
            break;
        case EMovementStatus::EMS_Sprinting:
            GetCharacterMovement()->MaxWalkSpeed = SprintingSpeed;
            break;
    }
}
FRotator AMainCharacter::Get_Look_At_Rotation_Yaw(FVector const& TargetLocation)
{
    FRotator PlayerToTargetRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), TargetLocation);
    FRotator PlayerToTargetRotationYaw = FRotator(0.f, PlayerToTargetRotation.Yaw, 0.f);

    return PlayerToTargetRotationYaw;
}

void AMainCharacter::Handle_Stamina_Change(float DeltaTime)
{
    switch (MovementStatus)
    {
    case EMovementStatus::EMS_Walking:
        Regenerate_Stamina(DeltaTime);

        if(Stamina >= TiredStamina)
        {
            Set_StaminaStatus(EStaminaStatus::ESS_Rested);
        }
        else
        {
            Set_StaminaStatus(EStaminaStatus::ESS_ExhaustedRecovering);
        }
        break;
    
    case EMovementStatus::EMS_Sprinting:
        Drain_Stamina(DeltaTime);

        if(Stamina <= 0.f)
        {
            Set_StaminaStatus(EStaminaStatus::ESS_Exhausted);
        }
        else if(Stamina <= TiredStamina)
        {
            Set_StaminaStatus(EStaminaStatus::ESS_Tired);
        }
        break;

    default:
        break;
    }
}
void AMainCharacter::Regenerate_Stamina(float DeltaTime)
{
    float DeltaStaminaRecovery = StaminaRecovery * DeltaTime;

    Stamina += DeltaStaminaRecovery;
    Stamina = FMath::Clamp<float> (Stamina, 0, MaxStamina);
}
void AMainCharacter::Drain_Stamina(float DeltaTime)
{
    float DeltaStaminaDrain = StaminaDrain * DeltaTime;

    Stamina -= DeltaStaminaDrain;
    Stamina = FMath::Clamp<float> (Stamina, 0, MaxStamina);
}



void AMainCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    check(PlayerInputComponent);

    PlayerInputComponent->BindAction(FName("Jump"), IE_Pressed, this, &AMainCharacter::Jump);
    PlayerInputComponent->BindAction(FName("Jump"), IE_Released, this, &ACharacter::StopJumping);
    PlayerInputComponent->BindAction(FName("Sprint"), IE_Pressed, this, &AMainCharacter::ShiftKey_Down);
    PlayerInputComponent->BindAction(FName("Sprint"), IE_Released, this, &AMainCharacter::ShiftKey_Up);

    PlayerInputComponent->BindAction(FName("Attack"), IE_Pressed, this, &AMainCharacter::Start_Attacking);
    PlayerInputComponent->BindAction(FName("Attack"), IE_Released, this, &AMainCharacter::Stop_Attacking);
    PlayerInputComponent->BindAction(FName("UseItem"), IE_Pressed, this, &AMainCharacter::Use_Item);


    PlayerInputComponent->BindAxis(FName("MoveForward"), this, &AMainCharacter::Move_Forward);
    PlayerInputComponent->BindAxis(FName("MoveRight"), this, &AMainCharacter::Move_Right);
    PlayerInputComponent->BindAxis(FName("TurnRightRate"), this, &AMainCharacter::Turn_At_Rate);
    PlayerInputComponent->BindAxis(FName("LookUpRate"), this, &AMainCharacter::LookUp_At_Rate);

    PlayerInputComponent->BindAxis(FName("Turn"), this, &APawn::AddControllerYawInput);
    PlayerInputComponent->BindAxis(FName("LookUp"), this, &APawn::AddControllerPitchInput);
}

void AMainCharacter::Move_Forward(float Value)
{
    if(MovementStatus == EMovementStatus::EMS_Dead) { return; }

    if(Controller && Value != 0 && !bAttacking)
    {
        const FRotator ControllerRotation = Controller->GetControlRotation();
        const FRotator YawRotation(0.f, ControllerRotation.Yaw, 0.f);

        const FVector ControllerForwardVector = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
        AddMovementInput(ControllerForwardVector, Value);
    }
}
void AMainCharacter::Move_Right(float Value)
{
    if(MovementStatus == EMovementStatus::EMS_Dead) { return; }

    if(Controller && Value != 0 && !bAttacking)
    {
        const FRotator ControllerRotation = Controller->GetControlRotation();
        const FRotator YawRotation(0.f, ControllerRotation.Yaw, 0.f);

        const FVector ControllerRightVector = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
        AddMovementInput(ControllerRightVector, Value);
    }
}

void AMainCharacter::Turn_At_Rate(float Rate)
{
    if(MovementStatus == EMovementStatus::EMS_Dead) { return; }

    if(GetWorld() && Rate != 0)
    {
        AddControllerYawInput(BaseTurnRate * Rate * GetWorld()->GetDeltaSeconds());
    }
}
void AMainCharacter::LookUp_At_Rate(float Rate)
{
    if(MovementStatus == EMovementStatus::EMS_Dead) { return; }
    
    if(GetWorld() && Rate != 0)
    {
        AddControllerPitchInput(BaseLookUpRate * Rate * GetWorld()->GetDeltaSeconds());
    }
}
void AMainCharacter::Jump()
{
    if(MovementStatus == EMovementStatus::EMS_Dead) { return; }

    Super::Jump();
}


float AMainCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
    Health -= DamageAmount;
    Health = FMath::Clamp<float> (Health, 0.f, MaxHealth);

    if(Health <= 0.f)
    {
        if(DamageCauser)
        {
            AEnemyCharacter* Enemy = Cast<AEnemyCharacter> (DamageCauser);
            if(Enemy)
            {
                Enemy->bHasValidTarget = false;
            }
        }
        Character_Killed();
    }

    return DamageAmount;
}
void AMainCharacter::Decrement_Health(float Damage)
{
    Health -= Damage;

    if(Health <= 0.f)
    {
        Character_Killed();
    }
}
void AMainCharacter::Character_Killed()
{
    if(MovementStatus == EMovementStatus::EMS_Dead) { return; }
    
    Set_MovementStatus(EMovementStatus::EMS_Dead);

    UAnimInstance* AnimationInstance = GetMesh()->GetAnimInstance();
    if(AnimationInstance)
    {
        if(bMontageHaveDeathSection && CombatMontageSections.Num() != 0)
        {
            AnimationInstance->Montage_Play(CombatMontage, 1.2f);
            AnimationInstance->Montage_JumpToSection(CombatMontageSections[DeathSectionNumber], CombatMontage);
        }
    }


}
void AMainCharacter::Death_End()
{
    if(!GetMesh()) { return; }

    GetMesh()->bPauseAnims = true;
    GetMesh()->bNoSkeletonUpdate = true;
}


void AMainCharacter::Increment_Coins(uint8 CoinNumber)
{
    Coins += CoinNumber;
}

void AMainCharacter::ShiftKey_Up()
{
    bShiftDown = false;
}
void AMainCharacter::ShiftKey_Down()
{
    if(GetMovementComponent()->GetLastInputVector() != FVector(0.f))
    bShiftDown = true;
}

void AMainCharacter::Start_Attacking()
{
    if(MovementStatus == EMovementStatus::EMS_Dead) { return; }
    if(bAttacking) { return; }
    if(!Is_Armed()) { return; }

    Set_InterpolateToEnemy(true);
    Attack();
}
void AMainCharacter::Attack()
{
    StartAttacking = true;
    bAttacking = true;

    UAnimInstance* AnimationInstance = GetMesh()->GetAnimInstance();
    if(AnimationInstance && CombatMontageSections.Num() != 0)
    {
        int32 NumberOfSections = CombatMontageSections.Num();
        if(bMontageHaveDeathSection)    NumberOfSections--;
        
        uint8 SectionNumber = FMath::RandRange(0, NumberOfSections - 1);

        AnimationInstance->Montage_Play(CombatMontage, 1.15f);
        AnimationInstance->Montage_JumpToSection(CombatMontageSections[SectionNumber], CombatMontage);
    }
}
void AMainCharacter::Stop_Attacking()
{
    StartAttacking = false;
}
void AMainCharacter::Attack_End()
{
    bAttacking = false;
    Set_InterpolateToEnemy(false);
    if(StartAttacking)
    {
        Start_Attacking();
    }
}

void AMainCharacter::Use_Item()
{
    if(ActiveOverlappingItem)
    {
        AEquipment* ItemToEquip = Cast<AEquipment> (ActiveOverlappingItem);

        if(ItemToEquip)
        {
            ItemToEquip->Equip(this);
        }
    }
}
void AMainCharacter::Set_Equipment(AEquipment* Equipment, EEquipmentType Type)
{
    if(!Equipment) { return; }

    switch (Type)
    {
    case EEquipmentType::EET_Weapon:
        if(EquippedWeapon)
        {
            EquippedWeapon->Destroy();
        }
        {
            AWeapon* WeaponToEquip = Cast<AWeapon> (Equipment);
            EquippedWeapon = WeaponToEquip;
        }
        break;

    case EEquipmentType::EET_Shield:
        if(EquippedShield)
        {
            EquippedShield->Destroy();
        }
        {
            AShield* ShieldToEquip = Cast<AShield> (Equipment);
            EquippedShield = ShieldToEquip;
        }
        break;

    default:
        break;
    }
}


void AMainCharacter::Spawn_HitParticles(FVector const& SpawnLocation, FRotator const& SpawnRotation) const
{
	if(!HitParticles) { return; }

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticles, SpawnLocation, SpawnRotation, true);
}
void AMainCharacter::Play_HitSound() const
{
	if(!HitSound) { return; }

	UGameplayStatics::PlaySound2D(this, HitSound);
}

bool AMainCharacter::Is_Armed()
{
    return (EquippedWeapon && EquippedShield);
}

void AMainCharacter::Update_CombatTarget()
{
    TArray<AActor*> OverlappingActors;
    GetOverlappingActors(OverlappingActors, AEnemyCharacter::StaticClass());

    if(OverlappingActors.Num() == 0) 
    { 
        if(PlayerController)
        {
            PlayerController->Display_EnemyHealthBar(false);
        }
        return; 
    }

    FVector PlayerLocation = GetActorLocation();
    float MinDistance = (OverlappingActors[0]->GetActorLocation() - PlayerLocation).Size();
    AEnemyCharacter* ClosestEnemy = Cast<AEnemyCharacter> (OverlappingActors[0]);
    for(AActor* OverlappingEnemy : OverlappingActors)
    {
        float DistanceToActor = (OverlappingEnemy->GetActorLocation() - GetActorLocation()).Size();
        if(DistanceToActor < MinDistance)
        {
            MinDistance = DistanceToActor;
            ClosestEnemy = Cast<AEnemyCharacter> (OverlappingEnemy);
        }
    }

    Set_CombatTarget(ClosestEnemy);
    
    if(PlayerController)
    {
        PlayerController->Display_EnemyHealthBar(true);
    }
}
