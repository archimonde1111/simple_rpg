// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "Components/BoxComponent.h"
#include "EnemyCharacter.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"


AWeapon::AWeapon()
{
    EquipmentSocketName = FName("WeaponSocket");
    EquipmentState = EEquipmentState::EES_Pickup;
    EquipmentType = EEquipmentType::EET_Weapon;


    CombatCollision = CreateDefaultSubobject<UBoxComponent> (FName("CombatCollision"));
    CombatCollision->SetupAttachment(GetRootComponent());

    CombatCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    CombatCollision->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
    CombatCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    CombatCollision->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
}


void AWeapon::BeginPlay()
{
    Super::BeginPlay();

    if(CombatCollision)
    {
        CombatCollision->OnComponentBeginOverlap.AddDynamic(this, &AWeapon::Combat_Collision_Component_Begin_Overlap);
        CombatCollision->OnComponentEndOverlap.AddDynamic(this, &AWeapon::Combat_Collision_Component_End_Overlap);
    }
}


void AWeapon::Collision_Component_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
    Super::Collision_Component_Begin_Overlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
}
void AWeapon::Collision_Component_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
    Super::Collision_Component_End_Overlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);
}


void AWeapon::Combat_Collision_Component_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
    if(!OtherActor) { return; }

    AEnemyCharacter* Enemy = Cast<AEnemyCharacter> (OtherActor);
    if(Enemy)
    {
        const USkeletalMeshSocket* BloodSplashSocket = EquipmentSkeletalMesh->GetSocketByName(FName("BloodSplash"));
        if(BloodSplashSocket)
        {
            FVector SplashLocation = BloodSplashSocket->GetSocketLocation(EquipmentSkeletalMesh);
            FRotator SplashRotation = BloodSplashSocket->GetSocketTransform(EquipmentSkeletalMesh).Rotator();
            Enemy->Spawn_HitParticles(SplashLocation, SplashRotation);
        }
        
        Enemy->Play_HitSound(); 

        if(DamageTypeClass)
        {
            UGameplayStatics::ApplyDamage(Enemy, Damage, EquipmentInstigator, this, DamageTypeClass);
        }
    }

}
void AWeapon::Combat_Collision_Component_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{}


void AWeapon::Enable_Combat_Collision()
{
    CombatCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}
void AWeapon::Disable_Combat_Collision()
{
    CombatCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AWeapon::Play_SwingSound()
{
    if(!SwingSound) { return; }

    UGameplayStatics::PlaySound2D(this, SwingSound);
}