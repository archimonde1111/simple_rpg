// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnVolume.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "EnemyCharacter.h"
#include "AIController.h"


ASpawnVolume::ASpawnVolume()
{
	PrimaryActorTick.bCanEverTick = true;

	SpawningBox = CreateDefaultSubobject<UBoxComponent> (FName("SpawningBox"));
}

void ASpawnVolume::BeginPlay()
{
	Super::BeginPlay();

}

void ASpawnVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


FVector ASpawnVolume::Get_Random_Spawn_Point()
{
	FVector Extent = SpawningBox->GetScaledBoxExtent();
	FVector Origin = SpawningBox->GetComponentLocation();

	FVector RandomPointInBox = UKismetMathLibrary::RandomPointInBoundingBox(Origin, Extent);
	return RandomPointInBox;
}

TSubclassOf<AActor> ASpawnVolume::Get_Random_Actor_To_Spawn()
{
	if(ActorsToSpawn.Num() == 0) { return nullptr; }

	uint8 RandomIndex = FMath::RandRange(0, ActorsToSpawn.Num() - 1);

	return ActorsToSpawn[RandomIndex];
}

void ASpawnVolume::Spawn_Actor_Implementation(UClass* ClassToSpawn, const FVector& Location)
{
	if(!ClassToSpawn) { return; }

	UWorld* World = GetWorld();
	if(!World) { return; }


	FActorSpawnParameters SpawnParameters;
	AActor* SpawnedActor = World->SpawnActor<AActor> (ClassToSpawn, Location, FRotator(0.f), SpawnParameters);

	AEnemyCharacter* EnemyActor = Cast<AEnemyCharacter> (SpawnedActor);
	if(EnemyActor) 
	{
		EnemyActor->SpawnDefaultController();

		AAIController* AIController = Cast<AAIController> ( EnemyActor->GetController());
		if(AIController)
		{
			EnemyActor->Set_Reference_To_AIController(AIController);
		}
	}
}