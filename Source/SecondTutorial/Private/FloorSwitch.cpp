// Fill out your copyright notice in the Description page of Project Settings.


#include "FloorSwitch.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "TimerManager.h"


AFloorSwitch::AFloorSwitch()
{
	PrimaryActorTick.bCanEverTick = true;

	FloorSwitchTriggerBox = CreateDefaultSubobject<UBoxComponent> (FName("TriggerBox"));
	RootComponent = FloorSwitchTriggerBox;
	FloorSwitchTriggerBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	FloorSwitchTriggerBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	FloorSwitchTriggerBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);//Ingores every thing ergo won't do anything, optimized
	FloorSwitchTriggerBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	FloorSwitchTriggerBox->SetBoxExtent(FVector(60.f, 60.f, 30.f));

	FloorSwitch = CreateDefaultSubobject<UStaticMeshComponent> (FName("FloorSwitch"));
	FloorSwitch->SetupAttachment(GetRootComponent());
	DoorToOpen = CreateDefaultSubobject<UStaticMeshComponent> (FName("Door"));
	DoorToOpen->SetupAttachment(GetRootComponent());
}


void AFloorSwitch::BeginPlay()
{
	Super::BeginPlay();

	DoorInitialLocation = DoorToOpen->GetComponentLocation();
	FloorSwitchInitialLocation = FloorSwitch->GetComponentLocation();

	FloorSwitchTriggerBox->OnComponentBeginOverlap.AddDynamic(this, &AFloorSwitch::On_Overlap_Begin);
	FloorSwitchTriggerBox->OnComponentEndOverlap.AddDynamic(this, &AFloorSwitch::On_Overlap_End);
}
void AFloorSwitch::On_Overlap_Begin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GetWorldTimerManager().ClearTimer(SwitchHandle);
	Lower_FloorSwitch();
	Rise_Door();
}
void AFloorSwitch::On_Overlap_End(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	GetWorldTimerManager().SetTimer(SwitchHandle, this, &AFloorSwitch::Close_Door, SwitchTime);
}
void AFloorSwitch::Close_Door()
{
	Raise_FloorSwitch();
	Lower_Door();
}


void AFloorSwitch::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AFloorSwitch::Update_Door_Location(float Z)
{
	FVector NewLocation = DoorInitialLocation;
	NewLocation.Z += Z;
	DoorToOpen->SetWorldLocation(NewLocation);
}
void AFloorSwitch::Update_FloorSwitch_Location(float Z)
{
	FVector NewLocation = FloorSwitchInitialLocation;
	NewLocation.Z += Z;
	FloorSwitch->SetWorldLocation(NewLocation);
}