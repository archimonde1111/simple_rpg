// Fill out your copyright notice in the Description page of Project Settings.


#include "Explosive.h"
#include "MainCharacter.h"
#include "EnemyCharacter.h"
#include "Kismet/GameplayStatics.h"


AExplosive::AExplosive()
{

}

void AExplosive::Collision_Component_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
    Super::Collision_Component_Begin_Overlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

    if(OtherActor)
    {
        AMainCharacter* PlayerCharacter = Cast<AMainCharacter> (OtherActor);
        AEnemyCharacter* EnemyCharacter = Cast<AEnemyCharacter> (OtherActor);

        if(PlayerCharacter || EnemyCharacter)
        {
            if(OverlapParticles)
            {
                UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), OverlapParticles, GetActorLocation(), FRotator(0.f), true);
            }
            if(OverlapSound)
            {
                UGameplayStatics::PlaySound2D(this, OverlapSound);
            }

            UGameplayStatics::ApplyDamage(OtherActor, Damage, nullptr, this, DamageTypeClass);
            Destroy();
        }
    }
}

void AExplosive::Collision_Component_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
    Super::Collision_Component_End_Overlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);

}