// Fill out your copyright notice in the Description page of Project Settings.


#include "Shield.h"


AShield::AShield()
{
    EquipmentSocketName = FName("ShieldSocket");
    EquipmentState = EEquipmentState::EES_Pickup;
    EquipmentType = EEquipmentType::EET_Shield;
}