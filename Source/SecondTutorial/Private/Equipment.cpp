// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"


AEquipment::AEquipment()
{
    EquipmentSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent> (FName("EquipmentSkeletalMesh"));
    EquipmentSkeletalMesh->SetupAttachment(GetRootComponent());
}


void AEquipment::Collision_Component_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
    Super::Collision_Component_Begin_Overlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

    if(EquipmentState == EEquipmentState::EES_Pickup && OtherActor)
    {
        AMainCharacter* MainCharacter = Cast<AMainCharacter> (OtherActor);

        if(MainCharacter)
        {
            MainCharacter->Set_ActiveOverlappingItem(this);
        }
    }
}

void AEquipment::Collision_Component_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
    Super::Collision_Component_End_Overlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);

    if(OtherActor)
    {
        AMainCharacter* MainCharacter = Cast<AMainCharacter> (OtherActor);

        if(MainCharacter)
        {
            MainCharacter->Set_ActiveOverlappingItem(nullptr);
        }
    }
}


void AEquipment::Equip(AMainCharacter* MainCharacter)
{
    if(!MainCharacter) { return; }
    if(!EquipmentSkeletalMesh) { return; }

    EquipmentSkeletalMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
    EquipmentSkeletalMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
    EquipmentSkeletalMesh->SetSimulatePhysics(false);

    Set_EquipmentInstigator(MainCharacter->GetController());

    const USkeletalMeshSocket* EquipmentSocket = MainCharacter->GetMesh()->GetSocketByName(EquipmentSocketName);
    if(EquipmentSocket)
    {
        bRotate = false;
        EquipmentSocket->AttachActor(this, MainCharacter->GetMesh());
        MainCharacter->Set_Equipment(this, EquipmentType);
        MainCharacter->Set_ActiveOverlappingItem(nullptr);

        if(EquippingSound)
        {
            UGameplayStatics::PlaySound2D(this, EquippingSound);
        }

        if(!bEquipmentParticles)
        {
            IdleParticleSystem->Deactivate();
        }
    }
}
