// Fill out your copyright notice in the Description page of Project Settings.


#include "Collider.h"
#include "Components/SphereComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "ColliderMovementComponent.h"

ACollider::ACollider()
{
	PrimaryActorTick.bCanEverTick = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent> (FName("SphereComponent"));
	SphereComponent->InitSphereRadius(40.f);
	SetRootComponent(SphereComponent);

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent> (FName("MeshComponent"));
	MeshComponent->SetupAttachment(GetRootComponent());
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshComponentAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere'"));
	if(MeshComponentAsset.Succeeded())
	{
		MeshComponent->SetStaticMesh(MeshComponentAsset.Object);
		MeshComponent->SetRelativeLocation(FVector(0.f, 0.f, -40.f));
		MeshComponent->SetWorldScale3D(FVector(0.8f, 0.8f, 0.8f));
	}

	SpringArm = CreateDefaultSubobject<USpringArmComponent> (FName("SpringArmComponent"));
	SpringArm->SetupAttachment(GetRootComponent());
	SpringArm->SetRelativeRotation(FRotator(-30.f, 0.f, 0.f));
	SpringArm->TargetArmLength = 500.f;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = 3.0f;

	Camera = CreateDefaultSubobject<UCameraComponent> (FName("CameraComponent"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	OurMovementComponent = CreateDefaultSubobject<UColliderMovementComponent> (FName("OurMovementComponent"));
	OurMovementComponent->UpdatedComponent = GetRootComponent();
}

void ACollider::BeginPlay()
{
	Super::BeginPlay();
	
}

void ACollider::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FRotator NewActorRotation = GetActorRotation();
	NewActorRotation.Yaw += CameraInput.X;
	SetActorRotation(NewActorRotation);

	FRotator NewSpringArmRotation = SpringArm->GetComponentRotation();
	NewSpringArmRotation.Pitch += CameraInput.Y;
	NewSpringArmRotation.Pitch = FMath::Clamp(NewSpringArmRotation.Pitch, -80.f, -15.f);
	SpringArm->SetWorldRotation(NewSpringArmRotation);
}

void ACollider::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ACollider::Move_Forward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ACollider::Move_Right);

	PlayerInputComponent->BindAxis(TEXT("CameraPitch"), this, &ACollider::Camera_Pitch);
	PlayerInputComponent->BindAxis(TEXT("CameraYaw"), this, &ACollider::Camera_Yaw);
}

void ACollider::Move_Forward(float Axis)
{
	FVector Forward = GetActorForwardVector();

	if(OurMovementComponent)
	{
		OurMovementComponent->AddInputVector(Axis * Forward);
	}
}
void ACollider::Move_Right(float Axis)
{
	FVector Right = GetActorRightVector();

	if(OurMovementComponent)
	{
		OurMovementComponent->AddInputVector(Axis * Right);
	}
}

void ACollider::Camera_Yaw(float Axis)
{
	CameraInput.X = Axis;
}
void ACollider::Camera_Pitch(float Axis)
{
	CameraInput.Y = Axis;
}


UPawnMovementComponent* ACollider::GetMovementComponent() const
{
	return OurMovementComponent;
}