// Fill out your copyright notice in the Description page of Project Settings.


#include "FloatingPlatform.h"
#include "Components/StaticMeshComponent.h"
#include "TimerManager.h"

AFloatingPlatform::AFloatingPlatform()
{
	PrimaryActorTick.bCanEverTick = true;

	Platform = CreateDefaultSubobject<UStaticMeshComponent> (FName("Platform"));
}

void AFloatingPlatform::BeginPlay()
{
	Super::BeginPlay();
	
	StartLocation = GetActorLocation();
	EndLocation += StartLocation;


	GetWorldTimerManager().SetTimer(InterpTimer, this, &AFloatingPlatform::Toggle_Interping, InterpTime);
}

void AFloatingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(bInterping)
	{
		FVector CurrentLocation = GetActorLocation();
		FVector Interp = FMath::VInterpTo(CurrentLocation, EndLocation, DeltaTime, InterpSpeed);

		if(CurrentLocation.Equals(EndLocation, 10.f))
		{
			Swap_Vectors(StartLocation, EndLocation);
			Toggle_Interping();
			GetWorldTimerManager().SetTimer(InterpTimer, this, &AFloatingPlatform::Toggle_Interping, InterpTime);
		}

		SetActorLocation(Interp);
	}
	
	
}


void AFloatingPlatform::Toggle_Interping()
{
	bInterping = !bInterping;
}
void AFloatingPlatform::Swap_Vectors(FVector& FirstVector, FVector& SecondVector)
{
	FVector temp = FirstVector;
	FirstVector = SecondVector;
	SecondVector = temp;
}