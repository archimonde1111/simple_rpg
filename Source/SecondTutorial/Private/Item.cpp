// Fill out your copyright notice in the Description page of Project Settings.


#include "Item.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/World.h"


AItem::AItem()
{
	PrimaryActorTick.bCanEverTick = true;

	CollisionMesh = CreateDefaultSubobject<USphereComponent> (FName("CollisionMesh"));
	RootComponent = CollisionMesh;

	VisibleMesh = CreateDefaultSubobject<UStaticMeshComponent> (FName("VisibleMesh"));
	VisibleMesh->SetupAttachment(GetRootComponent());

	IdleParticleSystem = CreateDefaultSubobject<UParticleSystemComponent> (FName("IdleParticleSystem"));
	IdleParticleSystem->SetupAttachment(GetRootComponent());
}


void AItem::BeginPlay()
{
	Super::BeginPlay();
	
	CollisionMesh->OnComponentBeginOverlap.AddDynamic(this, &AItem::Collision_Component_Begin_Overlap);
	CollisionMesh->OnComponentEndOverlap.AddDynamic(this, &AItem::Collision_Component_End_Overlap);
}
void AItem::Collision_Component_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{}
void AItem::Collision_Component_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{}


void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(bRotate)
	{
		FRotator ActorRotation = GetActorRotation();
		ActorRotation.Yaw += RotationRate * DeltaTime;
		SetActorRotation(ActorRotation);
	}
}

