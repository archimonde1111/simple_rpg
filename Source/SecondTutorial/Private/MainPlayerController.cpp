// Fill out your copyright notice in the Description page of Project Settings.


#include "MainPlayerController.h"
#include "Blueprint/UserWidget.h"


void AMainPlayerController::BeginPlay()
{
    Super::BeginPlay();

    if(HUDOverlayAsset)
    {
        HUDOverlay = CreateWidget<UUserWidget> (this, HUDOverlayAsset);
        
        HUDOverlay->AddToViewport();
        HUDOverlay->SetVisibility(ESlateVisibility::Visible);
    }

    if(WidgetEnemyHealthBar)
    {
        EnemyHealthBar = CreateWidget<UUserWidget> (this, WidgetEnemyHealthBar);
        if(EnemyHealthBar)
        {
            EnemyHealthBar->AddToViewport();
            EnemyHealthBar->SetVisibility(ESlateVisibility::Hidden);
            bEnemyHealthBarVisible = false;
        }
    }
}

void AMainPlayerController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if(EnemyHealthBar)
    {
        FVector2D HealthBarPositionInViewport;
        ProjectWorldLocationToScreen(EnemyLocation, HealthBarPositionInViewport);
        HealthBarPositionInViewport.Y -= 80.f;
        HealthBarPositionInViewport.X -= 50.f;

        EnemyHealthBar->SetDesiredSizeInViewport(EnemyHealthBarSizeInViewport);
        EnemyHealthBar->SetPositionInViewport(HealthBarPositionInViewport);
    }
}


void AMainPlayerController::Display_EnemyHealthBar(bool ShouldDisplay)
{
    if(!EnemyHealthBar) { return; }

    if(ShouldDisplay)
    {
        EnemyHealthBar->SetVisibility(ESlateVisibility::Visible);
        bEnemyHealthBarVisible = true;
    }
    else
    {
        EnemyHealthBar->SetVisibility(ESlateVisibility::Hidden);
        bEnemyHealthBarVisible = false;
    }
}