// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"
#include "Components/SphereComponent.h"
#include "AIController.h"
#include "MainCharacter.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Animation/AnimInstance.h"
#include "TimerManager.h"
#include "MainPlayerController.h"



AEnemyCharacter::AEnemyCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	AgroSphere = CreateDefaultSubobject<USphereComponent> (FName("AgroSphere"));
	AgroSphere->SetupAttachment(GetRootComponent());
	AgroSphere->InitSphereRadius(500.f);
	AgroSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Ignore);

	AttackRangeSphere = CreateDefaultSubobject<USphereComponent> (FName("AttackRangeSphere"));
	AttackRangeSphere->SetupAttachment(GetRootComponent());
	AttackRangeSphere->InitSphereRadius(75.f);

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
}

void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();

	AIController = Cast<AAIController> (GetController());
	
	if(AgroSphere)
	{
		AgroSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemyCharacter::AgroSphere_Begin_Overlap);
		AgroSphere->OnComponentEndOverlap.AddDynamic(this, &AEnemyCharacter::AgroSphere_End_Overlap);
	}
	if(AttackRangeSphere)
	{
		AttackRangeSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemyCharacter::AttackRangeSphere_Begin_Overlap);
		AttackRangeSphere->OnComponentEndOverlap.AddDynamic(this, &AEnemyCharacter::AttackRangeSphere_End_Overlap);
	}	

	Setup_CombatCollisions_Properties();
}
void AEnemyCharacter::Setup_CombatCollisions_Properties()
{
	for(UBoxComponent* Box : CombatCollisions)
	{
		if(Box) 
		{
			Box->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			Box->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
			Box->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
			Box->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

			Box->OnComponentBeginOverlap.AddDynamic(this, &AEnemyCharacter::CombatCollision_Begin_Overlap);
			Box->OnComponentEndOverlap.AddDynamic(this, &AEnemyCharacter::CombatCollision_End_Overlap);
		}
	}
}

void AEnemyCharacter::AgroSphere_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if(Is_Dead()) { return; }
	if(!OtherActor) { return; }

	AMainCharacter* MainCharacter = Cast<AMainCharacter> (OtherActor);
	if(MainCharacter)
	{
		Move_To_Target(MainCharacter);
		bHasValidTarget = false;
	}
}
void AEnemyCharacter::AgroSphere_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if(!OtherActor) { return; }

	AMainCharacter* MainCharacter = Cast<AMainCharacter> (OtherActor);
	if(MainCharacter)
	{
		bHasValidTarget = false;

		Set_EnemyMovementStatus(EEnemyMovementStatus::EEMS_Idle);
		if(AIController)
		{
			AIController->StopMovement();
		}

		MainCharacter->Update_CombatTarget();
	}
}

void AEnemyCharacter::AttackRangeSphere_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if(!OtherActor) { return; }

	AMainCharacter* MainCharacter = Cast<AMainCharacter> (OtherActor);
	if(MainCharacter)
	{
		bHasValidTarget = true;
		CombatTarget = MainCharacter;
		bOverlappingAttackRangeSphere = true;
		
		MainCharacter->Update_CombatTarget();

		Start_Attack();
	}
}
void AEnemyCharacter::AttackRangeSphere_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if(Is_Dead()) { return; }
	if(!OtherActor) { return; }

	AMainCharacter* MainCharacter = Cast<AMainCharacter> (OtherActor);
	if(MainCharacter)
	{
		MainCharacter->Update_CombatTarget();
		bOverlappingAttackRangeSphere = false;
		if(EnemyMovementStatus != EEnemyMovementStatus::EEMS_Attacking)
		{
			Move_To_Target(MainCharacter);
		}

		GetWorldTimerManager().ClearTimer(AttackTimer);
	}
}

void AEnemyCharacter::CombatCollision_Begin_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if(!OtherActor) { return; }

    AMainCharacter* MainCharacter = Cast<AMainCharacter> (OtherActor);
    if(MainCharacter)
    {
        Play_VFX(*MainCharacter);
        MainCharacter->Play_HitSound();

		if(DamageTypeClass)
		{
			UGameplayStatics::ApplyDamage(MainCharacter, Damage, AIController, this, DamageTypeClass);
		} 
    }
}
void AEnemyCharacter::Play_VFX(const AMainCharacter& Character)
{
	FString SocketName = FString("BloodSplash_Socket") + FString::FromInt(SectionToPlayNumber);//TODO When crit attack occurs blood spurts from one socket

	const USkeletalMeshSocket* BloodSplashSocket = GetMesh()->GetSocketByName(FName(SocketName));
	if(BloodSplashSocket)
	{
		FVector SplashLocation = BloodSplashSocket->GetSocketLocation(GetMesh());
		FRotator SplashRotation = BloodSplashSocket->GetSocketTransform(GetMesh()).Rotator();
		Character.Spawn_HitParticles(SplashLocation, SplashRotation);
	}
}
void AEnemyCharacter::CombatCollision_End_Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{}


void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float AEnemyCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	Decrement_Health(DamageAmount, DamageCauser);
	
	return DamageAmount;
}
void AEnemyCharacter::Decrement_Health(float DamageTaken, AActor* DamageCauser)
{
	DamageTaken = FMath::Clamp<float> (DamageTaken, 0, MaxHealth);

	Health -= DamageTaken;
	if(Health <= 0)
	{
		Character_Killed(DamageCauser);
	}
}
void AEnemyCharacter::Character_Killed(AActor* Killer)
{
	UAnimInstance* AnimationInstance = GetMesh()->GetAnimInstance();
	if(AnimationInstance)
	{
		if(bMontageHaveDeathSection && CombatMontageSections.Num() != 0)
		{
			AnimationInstance->Montage_Play(CombatMontage, 1.2f);
			AnimationInstance->Montage_JumpToSection(CombatMontageSections[DeathSectionNumber], CombatMontage);
		}
	}
	
	Set_EnemyMovementStatus(EEnemyMovementStatus::EEMS_Dead);
	for(UBoxComponent* Box : CombatCollisions)
	{
		Box->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	AgroSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	AttackRangeSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	AMainCharacter* MainCharacter = Cast<AMainCharacter> (Killer);
	if(!MainCharacter) { return; }
	MainCharacter->Update_CombatTarget();
}



void AEnemyCharacter::Move_To_Target(AMainCharacter* Target)
{
	if(!AIController) { return; }
	
	Set_EnemyMovementStatus(EEnemyMovementStatus::EEMS_MoveToTarget);

	FAIMoveRequest MoveRequest;
	MoveRequest.SetGoalActor(Target);
	MoveRequest.SetAcceptanceRadius(25.f);

	FNavPathSharedPtr NavPath;
	AIController->MoveTo(MoveRequest, &NavPath);
}


void AEnemyCharacter::Spawn_HitParticles(FVector const SpawnLocation, FRotator const SpawnRotation)
{
	if(!HitParticles) { return; }

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticles, SpawnLocation, SpawnRotation, true);
}
void AEnemyCharacter::Play_HitSound()
{
	if(!HitSound) { return; }

	UGameplayStatics::PlaySound2D(this, HitSound);
}


void AEnemyCharacter::Enable_Combat_Collision()
{
	for(UBoxComponent* Box : CombatCollisions)
	{
		if(!Box) { return; }
		Box->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
}
void AEnemyCharacter::Disable_Combat_Collision()
{
	for(UBoxComponent* Box : CombatCollisions)
	{
		if(!Box) { return; }
		Box->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}


void AEnemyCharacter::Start_Attack()
{
	if(Is_Dead() || !bHasValidTarget) { return; }
	
	if(AIController)
	{
		AIController->StopMovement();
		Set_EnemyMovementStatus(EEnemyMovementStatus::EEMS_Attacking);
	}

	if(!bAttacking)
	{
		bAttacking = true;
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if(AnimInstance)
		{
			uint8 NumberOfSections = CombatMontageSections.Num();
			if(NumberOfSections == 0) 	return;

			uint8 NumberOfNormalAttackAnimations = NumberOfSections;

			if(bMontageHaveDeathSection)	NumberOfNormalAttackAnimations--;
			if(bHaveComboAttack)	NumberOfNormalAttackAnimations--;

			Draw_Attack_Type(*AnimInstance, NumberOfNormalAttackAnimations);
		}
	}
}
void AEnemyCharacter::Draw_Attack_Type(UAnimInstance& AnimInstance, uint8 const& NumberOfNormalAttackAnimations)
{
	uint8 CriticalChance = FMath::RandRange(0, 4);
	if(bHaveComboAttack && (CriticalChance == 4))
	{
		AnimInstance.Montage_Play(CombatMontage, AttackAnimationSpeed);
		AnimInstance.Montage_JumpToSection(CombatMontageSections[ComboSectionNumber], CombatMontage);
	}
	else
	{
		SectionToPlayNumber = FMath::RandRange(0, NumberOfNormalAttackAnimations-1);

		AnimInstance.Montage_Play(CombatMontage, AttackAnimationSpeed);
		AnimInstance.Montage_JumpToSection(CombatMontageSections[SectionToPlayNumber], CombatMontage);
	}
}


void AEnemyCharacter::Attack_End()
{	
	bAttacking = false;

	if(bOverlappingAttackRangeSphere)
	{
		float TimeToNextAttack = FMath::FRandRange(MinimumTimeBetweenAttacks, MaximumTimeBetweenAttacks);
		GetWorldTimerManager().SetTimer(AttackTimer, this, &AEnemyCharacter::Start_Attack, TimeToNextAttack, false);
	}
	else
	{
		if(CombatTarget)
		{
			Move_To_Target(CombatTarget);
		}
	}
}

void AEnemyCharacter::Play_SwingSound()
{
	if(SwingSound)
	{
		UGameplayStatics::PlaySound2D(this, SwingSound);
	}
}

bool AEnemyCharacter::Is_Dead()
{
	return EnemyMovementStatus == EEnemyMovementStatus::EEMS_Dead;
}

void AEnemyCharacter::Death_End()
{
	GetMesh()->bPauseAnims = true;
	GetMesh()->bNoSkeletonUpdate = true;

	GetWorldTimerManager().SetTimer(DeathTimer, this, &AEnemyCharacter::Disappear, DeathDelay);
}
void AEnemyCharacter::Disappear()
{
	Destroy();
}
